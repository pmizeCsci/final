import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.util.List;
import java.awt.geom.Path2D;
/**
 * The Swimmer class consists of swimmer objects that randomly swim along the game field.
 * 
 * @author pmize
 * @version 12.14.2014
 */
public class Swimmer extends SmoothMover
{
    /*
     * The frameCount is defined for the frame count of an animation. 
     * The imageIndex is defined for the current index in an array. 
     */
    private int frameCount;
    private int imageIndex;
    private static final double SWIM_SPEED = 0.5;
    private GreenfootImage[] images = new GreenfootImage[8];
    private SpriteSheet spritesheet = new SpriteSheet();
    public Swimmer()
    {
        // The imageCount and frameCount variables are initialized to 0.
        imageIndex = 0;
        frameCount = 0;
        images[0] = new GreenfootImage( spritesheet.getSprite(new GreenfootImage("swimSpriteSheet.png"), 100, 0, 150, 50, 50, 50) );
        images[1] = new GreenfootImage( spritesheet.getSprite(new GreenfootImage("swimSpriteSheet.png"), 150, 0, 200, 50, 50, 50) );
        images[2] = new GreenfootImage( spritesheet.getSprite(new GreenfootImage("swimSpriteSheet.png"), 200, 0, 250, 50, 50, 50) );
        images[3] = new GreenfootImage( spritesheet.getSprite(new GreenfootImage("swimSpriteSheet.png"), 250, 0, 300, 50, 50, 50) );
        images[4] = new GreenfootImage( spritesheet.getSprite(new GreenfootImage("swimSpriteSheet.png"), 300, 0, 450, 50, 50, 50) );
        images[5] = new GreenfootImage( spritesheet.getSprite(new GreenfootImage("swimSpriteSheet.png"), 350, 0, 500, 50, 50, 50) );
        images[6] = new GreenfootImage( spritesheet.getSprite(new GreenfootImage("swimSpriteSheet.png"), 400, 0, 550, 50, 50, 50) );
        images[7] = new GreenfootImage( spritesheet.getSprite(new GreenfootImage("swimSpriteSheet.png"), 450, 0, 600, 50, 50, 50) );

    }//end Swimmer constructor

    /**
     * Act - do whatever the Swimmer wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {   
        // frameCount is post incremented.
        frameCount++;
        /* When frameCount reaches the size of the image array, calls the updateImage() method and resets frameCount. */
        if ( frameCount == 8 )
        {
            updateImage();
            frameCount = 0;
        }//end if
        // Calls the methods of Swimmer class in the act method.
        move();
        randomTurn();
        moveAtEdge();
    }//end act method

    /**
     * When this method is called, it updates the actor object's
     * image to whatever is the next frame in the images array
     * 
     * If the imageIndex exceeds the highest index in the array,
     * then the imageIndex is reset to zero
     */
    public void updateImage()
    {
        //stop();
        // imageIndex is the array index that corresponds to the corresponds
        // to the current image that we want to display
        setImage( images[imageIndex] );

        // if we're at the last animation frame, reset the image index
        // back to zero
        if (++imageIndex > 3)
        {
            imageIndex = 0;

        } // end if

    } // end method updateImage

    /** This definition controls which direction the swimmer will move (left or right)
     * by a random amount of degrees generated 
     * by calling the class method (using dot notation) Greenfoot.getRandomNumber().
     */
    public void randomTurn()
    {
        if ( Greenfoot.getRandomNumber(100) < 10 )
        {
            setLocation(getX()+0.25,getY()+0.25); //Max: 45 degrees Min: -45 degrees
        }//end if
    }//end randomTurn()

    /**
     * Move forward in the current direction.
     */
    public void move()
    {
        double angle = Math.toRadians( getRotation() );
        int x = (int) Math.round(getX() + Math.cos(angle) * SWIM_SPEED);
        int y = (int) Math.round(getY() + Math.sin(angle) * SWIM_SPEED);

        setLocation(x, y);
    }//end move method 

    /**
     * When reaching an edge, the swimmer will spawn at the opposite side.
     * Dividing current world width by current x value where swimmer is at an edge will result in 
     * 1 or a very small fraction, and in either case, the swimmer should spawn back at the same Y 
     * in an x value around 30-31.
     */
    public void moveAtEdge()
    {
        if ( atWorldEdge() )
        {
            setLocation(0,0) ;
        }//end if
    }//end moveAtEdge()

    /**
     * Test if we are close to one of the edges of the world. Return true if true.
     * Edges need to be more well defined.
     */
    public boolean atWorldEdge()
    {
        if(getX() < 20 || getX() > getWorld().getWidth() - 20)
            return true;
        if(getY() < 20 || getY() > getWorld().getHeight() - 20)
            return true;
        else
            return false;
    }//end atWorldEdge method

    /**
     * Turn 'angle' degrees towards the right (clockwise).
     */
    public void turn(int angle)
    {
        setRotation(getRotation() + angle);
    }//end turn 

    /**
     * Adds a point to the path by moving to the specified coordinates specified in double precision.
     */
    public void moveTo(int x, int y)
    {
        //addForce(Vector( deltaX, deltaY ));
    }//end moveTo method
}//end Swimmer class
