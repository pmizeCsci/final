import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
import java.util.ArrayList;
/**
 * This class is used to allow bee objects to behave like a bee based on existing methods and SmoothMover
 * along with the Actor classes. 
 * 
 * @author pmize, bcanada
 * @version 12.14.2014
 */
public class Bee extends SmoothMover
{   
    /**
     * Constructor for Bee class
     */
    public Bee()
    {

    }//end Bee constructor    
    /*
     * If there are swimmers in the world, the bee moves to the swimmer. 
     */
    /** Act - do whatever the Bee wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Call to the findNearestSwimmer method.
        findNearestSwimmer();
        stingSwimmer();
    } //end act method
    
    /**
     * This method will check for an intersection between the x values in sandshark and x values of bee, and the 
     * force vector will be added to push bees from x to a certain change in x determined by the random number generation
     * and set location method.(needs work). 
     */
    public void intersectingShark()
    {
        List<SandShark> sandshark = new ArrayList();
        sandshark = getWorld().getObjects(SandShark.class);
        if(getX() == this.getX())
        {
            double x = getX();
            double y = getY();
            setLocation(getX() + Greenfoot.getRandomNumber(800), getY() + Greenfoot.getRandomNumber(800));
            double deltaX = getX();
            double deltaY = getY();
            // creates a new vector for the bee using deltaX, deltaY
            addForce( new Vector( deltaX, deltaY ) );
            move();
        }//end if 
    }//end intersectingShark method

    /**
     * Finds the nearest Swimmer objects and stores them in a list. Then, the swimmer nearest the bee
     * object will find its distance between and replace the currentDistance with bestDistance when 
     * finding one distance less than another. This will result in the bee moving across a vector to
     * follow the swimmer until reaching the swimmer and will continue to follow with this method. 
     * Code edited courtesy of Dr. Canada
     */
    public void findNearestSwimmer()
    {
        // Create a list of objects containing swimmers. 
        List<Swimmer> nearbySwimmers = getWorld().getObjects( Swimmer.class );
        // Looks for the closest swimmer in 999 px of the game field. 
        double bestDistance = 999;
        int indexOfClosestSwimmer = 0;
        //int i = 0;
        for(Swimmer swimmer : nearbySwimmers)
        {
            //i++;
            double currentDistance = distance( this.getX(), 
                    swimmer.getX(),
                    this.getY(),
                    swimmer.getY() );
            if(currentDistance <= bestDistance) 
            { 
                // Finds the current best distance. 
                bestDistance = currentDistance;
                // Sets the index of the closest swimmer. 
                indexOfClosestSwimmer = nearbySwimmers.indexOf( swimmer );
            }//end if
            // Gets rid of current movement vector for the bee. 
            stop();

            // prepare to update the movement for the bee
            // gets the x and y of the swimmer defined in the for loop that was determined
            // to be closest to the bee. Subtracts the swimmer from the bee to get the delta. 
            double deltaX = ( nearbySwimmers.get( indexOfClosestSwimmer ).getX() - this.getX() );
            double deltaY = ( nearbySwimmers.get( indexOfClosestSwimmer ).getY() - this.getY() );

            // creates a new vector for the bee using deltaX, deltaY
            addForce( new Vector( deltaX, deltaY ) );

            // Keep the current direction of the vector, but set the length at 1.0
            // (or whatever "speed" you want the Bee to move)
            getMovement().setLength( 1 );

            move();
        }//end for

        //System.out.printf("%n%d Swimmers totals:", i );
    }//end findNearestSwimmer method

    /**
     * This method represents the bee's ability to sting the swimmer and remove them from the 
     * game when they are stung.
     * All swimmers are found and within range of the bee, they will be removed (10 pixel radius)
     * and a bee noise will be played. 
     */
    public void stingSwimmer()
    {
        ArrayList<Swimmer> intersectingSwimmers = (ArrayList<Swimmer>)( getObjectsInRange( 10, Swimmer.class ) );
        for(Swimmer swimmer : intersectingSwimmers)
        {
            if(intersectingSwimmers != null)
            {
                // Plays the sound of a bee.
                Greenfoot.playSound("beeNoise.mp3");
                // Removes the swimmer as if he's been stung.
                getWorld().removeObjects(intersectingSwimmers);
            }//end if
        }//end for loop
    }//end stingSwimmer method

    /**
     * Uses the distance formula and 4 arguments to return distance. 
     */
    public double distance( double x1, double x2, double y1, double y2 )
    {
        return Math.sqrt( Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2) );
    }//end distance method

}//end Bee class
