import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/*
 * Unused comment
 */
//ArrayList of util package used for spawning random bees that can be stored and 
//removed from an array, and when a shark intersects with a bee, the bee will
//be removed, so this ArrayList allows the removal of elements from an array, which 
//is otherwise not capable when using any normal array.
//Needed because the array size of 0 ends the game. 
import java.util.ArrayList;
import java.util.List;
import java.awt.geom.Path2D;
/**
 * The OceanField is our World subclass and defines what happens in the world
 * during each pass through the game loop.
 * 
 * @author pmize
 * @version 12.14.2014
 */
public class OceanField extends World
{
    // Speed of the game is a set value of 50. Unused
    private static final int GAME_SPEED = 50;
    // Definition for the GreenfootSound object called bgMusic. 
    private GreenfootSound bgMusic;
    private GreenfootSound secretMusic;
    // Definition for an object called playerShark of the SandShark class.
    private SandShark playerShark;
    // Definition for a bee object.
    private Bee bee, bee2, bee3;
    // Definition for a swimmer object.
    private Swimmer swimmer, swimmer2, swimmer3;

    /**
     * OceanField constructor instantiates objects of this main class.
     * This initializes a new SandShark() object as playerShark, and it is added to
     * a specific location, and the background music is initialized to an mp3 file. 
     */
    public OceanField()
    {    

        /* Creates a new world with 560x550 cells with a cell size of 1x1 pixels. */
        super(1200, 800, 1, false);
        // Creates a new Bee object.
        bee = new Bee();
        bee2 = new Bee();
        bee3 = new Bee();
        /*
         * First intended wave of bees.
         */
        // Add the bee object to the field at random domain [-50, 150] x and exactly 300 y. 
        addObject(bee, -50 + Greenfoot.getRandomNumber(200), 300);
        // Add the bee object to the field at random domain [-100, 100] x and exactly 300 y. 
        addObject(bee2, -100 + Greenfoot.getRandomNumber(200), 300);
        // Add the bee object to the field at random domain [-150, 50] x and exactly 300 y. 
        addObject(bee3, -150 + Greenfoot.getRandomNumber(200), 300);

        /*
         * Initial of 3 swimmers are spawned on wave 1. 
         */
        swimmer = new Swimmer();
        swimmer2 = new Swimmer();
        swimmer3 = new Swimmer();
        // Add the swimmer object to the field. Sets the object at a random location between X = [70, 870] and 
        // Y = [400, 600] (Y range accounts for spawning within the body of water).
        addObject(swimmer, 70 + Greenfoot.getRandomNumber(800), 400 + Greenfoot.getRandomNumber(100)*2) ;
        addObject(swimmer, 140 + Greenfoot.getRandomNumber(800), 350 + Greenfoot.getRandomNumber(100)*2) ;
        addObject(swimmer, 210 + Greenfoot.getRandomNumber(800), 300 + Greenfoot.getRandomNumber(100)*2) ;
        // Creates a new SandShark object.
        playerShark = new SandShark();
        // Add the playerShark object to the field. Sets the object at a random location between X = [70, 570] and 
        // Y = [400, 600] (Y range accounts for spawning within the body of water).
        addObject(playerShark, 70 + Greenfoot.getRandomNumber(500), 400 + Greenfoot.getRandomNumber(100)*2);
        // Sets the background music as seaWaves.mp3
        bgMusic = new GreenfootSound("seaWaves.mp3");
        // Plays amazing hidden soundtrack (owned by The Astronauts - The Vanishing Of Ethan Carter).
        secretMusic = new GreenfootSound("secret.mp3");
    }//end OceanField() constructor

    /**
     * Act method for our OceanField subclass of World
     */
    public void act()
    {
        /*
         * If already playing, music will stop playing
         * when space key is pressed. Otherwise, the game will play a looping 
         * mp3 sound. 
         */
        if ( Greenfoot.isKeyDown("space") )
        {
            if ( bgMusic.isPlaying() )
            {
                bgMusic.stop();
            }//end if
            else 
            {
                bgMusic.playLoop();
            }//end else
        }//end if   
        else if ( Greenfoot.isKeyDown("x"))
        {
            if ( bgMusic.isPlaying() )
            {
                secretMusic.stop();
            }//end if
            else 
            {
                secretMusic.playLoop();
            }//end else
        }//end else
    }//end act method
    /**
     * 
     */
    public void lineTo()
    {
    }//end lineTo method
}//end OceanField class
