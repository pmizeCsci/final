import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.util.List;
/**
 * SandShark is the Actor subclass that defines what happens to any 
 * SandShark object created in the main class. All properties are passed from SmoothMover to SandShark.
 * Handles turning and movement of the SandShark.
 * 
 * @author pmize 
 * @version 12.14.2014
 */ 
public class SandShark extends SmoothMover
{
    private boolean facingCorrectly;
    // A new int type variable for the angle of the image. 
    private int angle; 
    /*
     * The frameCount is defined for the frame count of an animation. 
     * The imageIndex is defined for the current index in an array. 
     */
    private int frameCount;
    private int imageIndex;

    // Boolean variable declaration representing the image direction. 
    private boolean directionIsLeft;

    // Defines splash as a sound object. 
    private GreenfootSound splash;

    // This array declaration holds 4 elements from index 0 to index 3.
    private GreenfootImage[] images = new GreenfootImage[4]; //size is 4 (0-3) index

    // A new spritesheet object is declared. 
    private SpriteSheet spritesheet = new SpriteSheet();

    /**
     * SandShark constructor instantiates objects for the SandShark class.
     */
    public SandShark()
    {
        // The imageCount and frameCount variables are initialized to 0.
        imageIndex = 0;
        frameCount = 0;

        /* Variable directionIsLeft initially is assigned 1 for true in this constructor */
        directionIsLeft = true; 
        images[0] = new GreenfootImage( spritesheet.getSprite(new GreenfootImage("sharkSpriteSheet2.png"), 416, 0, 547, 69, 131, 69) );
        images[1] = new GreenfootImage( spritesheet.getSprite(new GreenfootImage("sharkSpriteSheet2.png"), 607, 0, 737, 69, 131, 69) );
        images[2] = new GreenfootImage( spritesheet.getSprite(new GreenfootImage("sharkSpriteSheet2.png"), 225, 0, 356, 69, 131, 69) );
        images[3] = new GreenfootImage( spritesheet.getSprite(new GreenfootImage("sharkSpriteSheet2.png"), 31, 0, 165, 69, 131, 69) );

        // Initializes the GreenfootSound object with the mp3 file. 
        splash = new GreenfootSound( "underwater.mp3" );
    }//end constructor SandShark()

    /**
     * Act - do whatever the SandShark wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        
        // frameCount is post incremented.
        frameCount++;
        /* When frameCount reaches the size of the image array, calls the updateImage() method and resets frameCount. */
        if ( frameCount == 4 )
        {
            updateImage();
            frameCount = 0;
        }//end if

        //         /*
        //          * For the index 0 to the length of the images array, frameCount is printed when the act() method is called, e.g. during run time. 
        //          */
        //         for(int i = 0; i < images.length; i++)
        //         {
        //             // Prints the frameCount
        //             System.out.println(frameCount);
        //         }//end for loop
        directionIsLeft = isFacingCorrectly(directionIsLeft);
        checkForKeyPress();
        mirrorFrames(images, facingCorrectly);
    }//end act method

    /**
     * When this method is called, it updates the actor object's
     * image to whatever is the next frame in the images array
     * 
     * If the imageIndex exceeds the highest index in the array,
     * then the imageIndex is reset to zero
     */
    public void updateImage()
    {
        //stop();
        // imageIndex is the array index that corresponds to the corresponds
        // to the current image that we want to display
        setImage( images[imageIndex] );

        // if we're at the last animation frame, reset the image index
        // back to zero
        if (++imageIndex > 3)
        {
            imageIndex = 0;

        } // end if

    } // end method updateImage

    /**
     * Moves the shark left and right (and flips the
     * image accordingly) in response to arrow key presses.
     * The shark will continue to move right or left until it has reached
     * either the left or right world edge. 
     * It can also move diagonally left and right while pressing up and
     * down keys in combination with left and right keys. 
     * Needs to add get and set rotation to simplify. 
     */
    public void checkForKeyPress()
    {
        if ( Greenfoot.isKeyDown("left") )
        {
            facingCorrectly = true;
            // (accounting for an 10.0-pixel "margin")
            move(-5); // move 5 pixels to the left
            makeDirectionLeft(directionIsLeft);
        } // end if

        else if ( Greenfoot.isKeyDown("right") )
        {       
            facingCorrectly = false;
            move(+5); // move 5 pixels to the right
            makeDirectionRight(directionIsLeft);
        } // end else if

        else if ( Greenfoot.isKeyDown("up"))
        {       
            turnUp(); 
        } // end else if

        else if (Greenfoot.isKeyDown("down"))
        {       
            turnDown();
        } // end else if

    }//end checkForKeyPress method

    /**
     * Turn 'angle' degrees towards the right (counter clockwise).
     */
    public void turnUp()
    { 
        int dAngle = 0;
        dAngle -= 45;
        if(dAngle == -45);
        {
            angle = dAngle;
            setRotation(angle);
        }//end if
    }//end turn 

    /**
     * Turn 'angle' degrees towards the right (clockwise).
     */
    public void turnDown()
    { 
        int dAngle = 0;
        dAngle += 45;
        if(dAngle == 45);
        {
            angle = dAngle;
            setRotation(angle);
        }//end if
    }//end turn 

    /**
     * Boolean value will represent a shark facing left.
     */
    public boolean makeDirectionLeft(boolean directionIsLeft)
    {
        directionIsLeft = true; // <-- now direction is left
        return directionIsLeft;
    }//end makeDirectionLeft method

    /**
     * Boolean value will represent a shark facing right.
     */
    public boolean makeDirectionRight(boolean directionIsLeft)
    {

        directionIsLeft = false; // <-- now direction is not left
        return directionIsLeft;
    }//end makeDirectionLeft method

    /**
     * Mirrors the frames of the animation to face the right directions.
     */
    public void mirrorFrames(GreenfootImage[] images, boolean canMirror)
    {
        if(canMirror)
        {
            for ( GreenfootImage image : images )
            {
                image.mirrorHorizontally();
            } // end for loop
        }//end if
    }//end mirrorImage method

    /**
     * Returns true if the shark is facing left. 
     */
    public boolean isFacingCorrectly(boolean directionIsLeft)
    {
        if(directionIsLeft)
        {
            facingCorrectly = true;
        }//end if
        else
        {
            facingCorrectly = false;
        }//end else
        return facingCorrectly;
    }//end else
    
}//end SandShark class

