/**
 * A 2D vector. The vector can be read and manipulated in Cartesian coordinates
 * (as an x,y-offset pair) or in polar coordinates (as a direction and a length).
 * Class is nearly similar, but entirely written by pmize one method at a time 
 * excluding uneccessary methods.. 
 * 
 * @author pmize, Brian Canada
 * @version 12.14.2014
 */
public final class Vector  
{
    // Defined 2 variables change in y, change in x. 
    double dy;
    double dx;
    // Defined 2 more variabls type int and double for direction and length.
    int direction;
    double length;

    /**
     * Creates a new neutral vector.
     */
    public Vector()
    {

    }//end Vector constructor

    /**
     * Create a vector with given direction and length. The direction should be in
     * the range [0..359], where 0 is EAST, and degrees increase clockwise.
     */
    public Vector(int direction, double length)
    {
        this.length = length;
        this.direction = direction;
        updateCartesian();
    }//end constructor Vector

    /**
     * Create a vector by specifying the x and y offsets from start to end points.
     */
    public Vector(double dx, double dy)
    {
        this.dx = dx;
        this.dy = dy;
        //updatePolar();
    }//end constructor Vector

    /**
     * Set this vector to the neutral vector (length 0).
     */
    public void setNeutral() {
        dx = 0.0;
        dy = 0.0;
        length = 0.0;
        direction = 0;
    }//end setNeutral method

    /**
     * Accessor method used to get direction returned as an int variable.
     */
    public int getDirection()
    {
        return direction;
    }//end getDirection method
    /**
     * Accessor method used to get length returned as an double variable.
     */
    public double getLength()
    {  
        return length;
    }//end getLength method
    /**
     * Mutator method assigns instance of direction provided as an int type argument 
     * to the local reference, this.direction when called, and the local now points to this 
     * value.
     */
    public void setDirection(int direction)
    {
        this.direction = direction;
        updateCartesian();
    }//end setDirection method
    /**
     * Mutator method assigns instance of length provided as an double type argument 
     * to the local reference, this.length when called, and the local now points to this 
     * value.
     */
    public void setLength(double length)
    {
        this.length = length;
        updateCartesian();
    }//end setLength method 

    /**
     * Get the x offset and return as dx (start to end point).
     */
    public double getX()
    {
        return dx;
    }//end getX method

    /**
     * Get the y iffset and return as dy (start to end point). 
     */
    public double getY()
    {
        return dy;
    }//end getY method

    /**
     * Add another Vector given argument 
     * Adds the Vector argument's offset values, dx and dy, to the new vector's offset,
     * other.dx and other.dy. updatePolar() updates direction and length of this vector
     * using the new value of dx and dy. 
     */
    public void add(Vector other)
    {
        dx += other.dx;
        dy += other.dy;
        updatePolar();
    }//end add method
    /**
     * Updates the direction and length of the vector accessing instances of dx and dy, and the
     * calculated values are assigned to references to the instance variables direction and length.
     */
    public void updatePolar()
    {
        this.direction = (int) Math.toDegrees(Math.atan2(dy, dx));
        this.length = Math.sqrt(dx*dx+dy*dy);
    }//end updatePolar method
    /**
     * dx and dy are given a new instance value calculated with cosine and sine of
     * direction multiplied by length, respectively.
     */
    public void updateCartesian()
    {
        dx = length * Math.cos(Math.toRadians(direction));
        dy = length * Math.sin(Math.toRadians(direction));   
    }//end updateCartesian method
}//end Vector class
